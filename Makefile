#!/usr/bin/make -f

draft = rfc3709bis
OUTPUT = $(draft).txt $(draft).html $(draft).xml $(draft).pdf
all: $(OUTPUT)

# Programs
KRAMDOWN := kramdown-rfc2629
XML2RFC := xml2rfc

%.xml: %.md
	$(KRAMDOWN) --v3 < $< > $@.tmp
	mv $@.tmp $@

%.html: %.xml
	$(XML2RFC) --v3 --html $< --out $@

%.pdf: %.xml
	$(XML2RFC) --v3 --pdf $< --out $@

%.txt: %.xml
	$(XML2RFC) --v3 --text $< --out $@

clean:
	-rm -rf $(OUTPUT) *.tmp

check:
	codespell $(draft).md

.PHONY: clean all check
