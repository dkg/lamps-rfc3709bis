# Media in X.509 certificates

This repository tracks the development of the revision to [RFC 3709](https://datatracker.ietf.org/doc/html/rfc3709).

The official IETF tracker for the document is at [draft-ietf-lamps-rfc3709bis](https://datatracker.ietf.org/doc/draft-ietf-lamps-rfc3709bis/).

This repository is currently hosted [by dkg on gitlab](https://gitlab.com/dkg/lamps-rfc3709bis), but this is not its official home (the authors have not annouced a public issue tracker or repository for it).
